FROM alpine:3.17
RUN apk add syncthing dumb-init
EXPOSE 22000 8384
VOLUME /data
VOLUME /config
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
ENTRYPOINT ["syncthing", "serve", "--no-browser", "--no-upgrade", "--config=/config"]
